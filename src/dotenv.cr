require "json"

# https://github.com/feifanzhou/crystal_dotenv/blob/master/src/crystal_dotenv.cr
File.each_line(".env") do |line|
  next if line.starts_with? '#'
  next unless line.includes? "="

  key, value = line.strip.split "=", 2

  if value.starts_with? "\""
    ENV[key] = JSON.parse(value).to_s
  else
    ENV[key] = value
  end
end if File.file? ".env"

# Convertir ENV a un hash aceptado por Process
env = Hash(String, String).new
ENV.each do |key, value|
  env[key] = value
end

# Correr el proceso con el .env cargado
Process.exec("/bin/sh", ["-l", "-c", ARGV.join(" ")], env: env, input: STDIN)
