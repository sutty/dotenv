dotenv: src/dotenv.cr
	crystal build --release $<
	strip --strip-all $@

install: dotenv
	install -Dm755 $< $(PREFIX)/usr/bin/$<
