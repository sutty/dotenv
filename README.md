# dotenv

Runs command line with `.env` loaded.

## Installation

Install [Crystal](https://crystal-lang.org/) only for compiling.

```sh
# Archlinux dependencies
sudo pacman -S base-devel crystal pcre libevent
# Compile
make dotenv
# Install
sudo make install
```

## Binary packages

### Gitlab Releases

[Download a release](https://0xacab.org/sutty/dotenv/-/releases).  You
may need to install the package for `libevent` in your distribution.

### Alpine

Add [Sutty repositories](https://alpine.sutty.nl/):

Alpine 3.14: <https://alpine.sutty.nl/alpine/v3.14/sutty>

Alpine 3.16: <https://alpine.sutty.nl/alpine/v3.16/sutty>

## Usage

Add environment variables to a `.env.` file on the current directory.

```sh
>_ echo "TEST=hi" >> .env
>_ dotenv 'echo $TEST'
hi
>_ dotenv echo \$TEST
hi
```

## Contributing

1. Fork it (<https://0xacab.org/sutty/dotenv>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [fauno](https://0xacab.org/fauno)
